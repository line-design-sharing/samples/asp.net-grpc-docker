using Grpc.Core;
using System.Threading.Tasks;
using System;
using Line.Samples.Net.GRPC.Protos;

namespace Line.Samples.Net.gRPC.Server;

public class GreeterServiceImpl : GreetingService.GreetingServiceBase
{
    public override Task<HelloResponse> Greeting(HelloRequest request, ServerCallContext context)
    {
        Console.WriteLine($"Message: {request.Name}");
        return Task.FromResult(new HelloResponse { Greeting = "Hello " + request.Name });
    }
}