using Grpc.Core;
using System.Threading.Tasks;
using System;
using Line.Samples.Net.GRPC.Protos;

namespace Line.Samples.Net.gRPC.Server;

public class CalculatorServiceImpl : CalculatorService.CalculatorServiceBase
{
    public override Task<CalculateResponse> Calculate(CalculateRequest request, ServerCallContext context)
    {
        Console.WriteLine($"Calculate: {request.VariableA} {request.Operation} {request.VariableB}");

        double? result = null;

        if (request.Operation == Operation.Addition)
            result = request.VariableA + request.VariableB;
        else if (request.Operation == Operation.Subtraction)
            result = request.VariableA - request.VariableB;
        else if (request.Operation == Operation.Multiplication)
            result = request.VariableA * request.VariableB;
        else if (request.Operation == Operation.Division)
            result = request.VariableA / request.VariableB;

        return Task.FromResult(new CalculateResponse() { Result = result });
    }
}