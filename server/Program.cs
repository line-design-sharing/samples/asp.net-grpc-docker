using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Line.Samples.Net.GRPC.Protos;

namespace Line.Samples.Net.gRPC.Server;

internal class Program
{
    const string Host = "0.0.0.0";
    const int Port = 5000;

    public static void Main(string[] args)
    {
        var server = new Grpc.Core.Server
        {
            Services =
            {
                GreetingService.BindService(new GreeterServiceImpl()),
                CalculatorService.BindService(new CalculatorServiceImpl())
            },
            Ports = { new ServerPort(Host, Port, ServerCredentials.Insecure) }
        };

        CancellationTokenSource tokenSource = new CancellationTokenSource();
        var serverTask = RunServiceAsync(server, tokenSource.Token);

        Console.WriteLine("GreeterServer listening on port " + Port);
        Console.WriteLine("Press any key to stop the server...");
        Console.Read();

        tokenSource.Cancel();
        Console.WriteLine("Shutting down...");
        serverTask.Wait();
    }

    private static Task AwaitCancellation(
        CancellationToken token)
    {
        var taskSource = new TaskCompletionSource<bool>();
        token.Register(() => taskSource.SetResult(true));
        return taskSource.Task;
    }

    private static async Task RunServiceAsync(
        Grpc.Core.Server server,
        CancellationToken cancellationToken = default(CancellationToken))
    {
        server.Start();

        await AwaitCancellation(cancellationToken);
        await server.ShutdownAsync();
    }
}